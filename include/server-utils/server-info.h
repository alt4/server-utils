#pragma once
#include <server-utils/config.h>

#include <string>
#include <vector>
#include <map>

struct ActorInfoStat
{
	std::string Name;
	int Kills;
	int Deaths;
	int Score;
};

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(ActorInfoStat,
	Name, Kills, Deaths, Score);

struct TeamInfoStat
{
	std::vector<ActorInfoStat> PlayerList = {};
	std::vector<ActorInfoStat> BotList = {};

	int PlayerCount = 0;
	int BotCount = 0;
	int TeamScore = 0;
	int TeamIndex = 0;
	std::string TeamName = "";
};

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(TeamInfoStat,
	PlayerList, BotList, PlayerCount, BotCount, TeamScore, TeamIndex, TeamName);

struct ServerInfo {
	std::vector<TeamInfoStat> TeamList;

	int PlayerCount;
	int BotCount;
	std::string Map;
	std::string ServerName;
	std::string GameMode;
	std::string GameModeFullName;
	std::string Playlist;
	int GoalScore;
	int TimeLimit;
	int RemainingTime;
	int MaxPlayers;

	ServerMutators Mutators;
};

NLOHMANN_DEFINE_TYPE_NON_INTRUSIVE(ServerInfo,
	TeamList, PlayerCount, BotCount, Map, ServerName, GameMode, GameModeFullName,
	Playlist, GoalScore, TimeLimit, RemainingTime, MaxPlayers, Mutators);