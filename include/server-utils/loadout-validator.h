#pragma once
#include <BLRevive/Component.h>
#include <server-utils/config.h>
#include <SdkHeaders.h>

class AFoxPC;
class AFoxLoadoutInfo;
class UFoxWeaponModObjectBase;
struct FFoxWeaponConfigInfo;
struct FProfileGearInfo;
enum EWeaponType;
class AFoxWeapon;
class UClass;
class ServerUtils;
class UFoxDataProvider_ModBase;
struct FFoxWeaponPresetInfo;

namespace BLRE
{
	using ModValidationMapping = std::map<int, std::vector<int>>;
	using ItemClassUIDMapping = std::map<UClass*, int>;

	class LoadoutValidator : Component
	{
	public:
		LoadoutValidator(ServerUtils* serverUtils) : Component("LoadoutValidator") 
		{
			this->serverUtils = serverUtils;
		}

		bool ValidatePlayerLoadout(AFoxPC* pc);

		void LoadValidationMapping();
		void LoadValidationRules();
		void BuildItemClassUIDMap();
	private:
		bool ValidateLoadoutWeapon(AFoxPC* pc, EWeaponType type, AFoxLoadoutInfo* loadoutInfo);
		bool ValidateLoadoutGear(AFoxPC* pc, FProfileGearInfo& gear);

		bool IsModValidForWeapon(UClass* modCls, AFoxWeapon* weapon);
		bool IsModValidForWeapon(int modUid, AFoxWeapon* weapon);
		bool IsModValidForWeapon(UFoxDataProvider_ModBase* modProvider, AFoxWeapon* weapon);

		bool IsItemDisabled(int itemUID);
		bool IsItemDisabled(UClass* itemClass);
		bool IsModDisabledForWeapon(int modUid, AFoxWeapon* weapon);
		bool IsModDisabledForWeapon(UClass* modCls, AFoxWeapon* weapon);

		FFoxWeaponPresetInfo GetSuitableWeaponPreset(EWeaponType type);

		int GetSuitableGearFromProviderArray(TArray<UUIResourceDataProvider*> providers);
	private:
		ServerUtils* serverUtils;
		ModValidationMapping ModValidationInfo = {};
		ItemClassUIDMapping ItemClassUIDMap = {};
		ValidationRules validationRules = {};
		std::map<std::string, AFoxWeapon*> WeaponNameMap = {};

		FFoxWeaponPresetInfo PrimaryPresetInfo = {};
		FFoxWeaponPresetInfo SecondaryPresetInfo = {};

		std::map<EArmorItem, int> DefaultArmorIds = {};
	};
}