#include <server-utils/loadout-validator.h>
#include <SdkHeaders.h>
#include <BLRevive/Resources.h>
#include <server-utils/resource.h>
#include <server-utils/config.h>
#include <server-utils/server-utils.h>
#include <filesystem>
using namespace BLRE;
namespace fs = std::filesystem;

void LoadoutValidator::LoadValidationMapping()
{
	Log->debug("load validation mapping from resources");
	nlohmann::json modValidRes = BLRE::Resources::Get<nlohmann::json>(RES_WEAPON_MOD_VALIDATION_MAPPING);
	auto map = modValidRes.get<std::map<std::string, std::vector<int>>>();
	for (auto e : map) {
		ModValidationInfo[std::stoi(e.first)] = e.second;
	}
}

void LoadoutValidator::LoadValidationRules()
{
	// load validation rules from json file
	std::string validationRulesFile = serverUtils->moduleConfig.RulesFile;
	Log->debug("load validation rules from {}", validationRulesFile);

	if (!fs::exists(validationRulesFile)) {
		Log->warn("validation rule config {} doesn't exist", validationRulesFile);

		std::string defaultValidationFile = serverUtils->moduleConfig.RulesPath + "default.json";
		if (!fs::exists(defaultValidationFile)) {
			Log->warn("default validation rule config {} doesn't exist, skipping", defaultValidationFile);
			return;
		}
		else {
			validationRulesFile = defaultValidationFile;
		}
	}

	try {
		validationRules = BLRE::Utils::ReadJsonFile<ValidationRules>(validationRulesFile);
	}
	catch (nlohmann::json::exception& e) {
		Log->error("failed to read validation rules from {} because of: {}", validationRulesFile, e.what());
	}
	catch (std::exception& e) {
		Log->error("failed to read validation rules from {} because of: {}", validationRulesFile, e.what());
	}

	// find and cache enabled weapon presets
	PrimaryPresetInfo = GetSuitableWeaponPreset(WT_Primary);
	SecondaryPresetInfo = GetSuitableWeaponPreset(WT_Secondary);
}

void LoadoutValidator::BuildItemClassUIDMap()
{
	Log->debug("build item class uid map");

	// get all weapons which have an associated UnlockID
	std::vector<AFoxWeapon*> weapons = UObject::GetInstancesOf<AFoxWeapon>(
		[](AFoxWeapon* weap) { 
			return weap->UnlockID > 0; 
		});
	// cache weapon infos
	for (AFoxWeapon* wpn : weapons) {
		ItemClassUIDMap[wpn->Class] = wpn->UnlockID;
		WeaponNameMap[wpn->Class->GetName()] = wpn;
	}

	// get all weapon mods which have an associated UnlockID
	std::vector<UFoxWeaponModObjectBase*> mods = UObject::GetInstancesOf<UFoxWeaponModObjectBase>(
		[](UFoxWeaponModObjectBase* modObj) {
			return modObj->UnlockID > 0;
		});
	// cache weapon mod infos
	for (UFoxWeaponModObjectBase* modObj : mods)
		ItemClassUIDMap[modObj->Class] = modObj->UnlockID;

	Log->debug("mapped {} item classes", ItemClassUIDMap.size());
}

bool LoadoutValidator::ValidatePlayerLoadout(AFoxPC* pc)
{
	AFoxPRI* pri = reinterpret_cast<AFoxPRI*>(pc->PlayerReplicationInfo);
	AFoxLoadoutInfo* loadoutInfo = pri->Loadout;

	Log->debug("validating loadout {} for player {}", loadoutInfo->CurrentLoadoutID.ToChar(), pri->PlayerName.ToChar());
	// @todo: check if current loadout already was validated and force last result

	bool hasChanged = false;

	hasChanged = ValidateLoadoutWeapon(pc, WT_Primary, loadoutInfo);
	hasChanged = ValidateLoadoutWeapon(pc, WT_Secondary, loadoutInfo) || hasChanged;
	hasChanged = ValidateLoadoutGear(pc, loadoutInfo->GearInfo) || hasChanged;
	
	if(hasChanged)
		loadoutInfo->bNetDirty = true;

	return hasChanged;
}

bool LoadoutValidator::ValidateLoadoutWeapon(AFoxPC* pc, EWeaponType type, AFoxLoadoutInfo* loadoutInfo)
{
	// get unlockables data store and infos
	static UFoxDataStore_Unlockables* uds = UObject::GetInstanceOf<UFoxDataStore_Unlockables>();
	static UFoxUnlockInfo* unlockInfo = UObject::GetInstanceOf<UFoxUnlockInfo>(true);

	AFoxPRI* pri = (AFoxPRI*)pc->PlayerReplicationInfo;
	auto weapon = type == WT_Primary ? &loadoutInfo->PrimaryWeaponInfo : &loadoutInfo->SecondaryWeaponInfo;
	auto wpnTypeStr = type == WT_Primary ? "primary" : "secondary";

	Log->debug("validating {} weapon for {} of {}", wpnTypeStr, loadoutInfo->CurrentLoadoutID.ToChar(), pri->PlayerName.ToChar());

	std::string playerLoadoutId = fmt::format("{}:{}", pri->PlayerName.ToChar(), loadoutInfo->CurrentLoadoutID.ToChar());
	std::string weaponName = weapon->WeaponClass->GetName();

	FFoxWeaponConfigInfo defaultConfig = {};
	FFoxWeaponPresetInfo defaultPreset = {};

	bool hasChanged = false;

	// check if weapon is disabled
	if (IsItemDisabled(weapon->WeaponClass)) {
		// set weapon to next enabled weapon using default presets
		defaultPreset = type == WT_Primary ? PrimaryPresetInfo : SecondaryPresetInfo;
		Log->warn("weapon {} is disabled for this server, resetting weapon config to preset {}", weapon->WeaponClass->GetName(), defaultPreset.NameID.ToChar());
		defaultConfig = loadoutInfo->BuildWeaponConfigInfoFromPresetName(defaultPreset.NameID);
		*weapon = defaultConfig;
		hasChanged = true;
	}
	else {
		unlockInfo->GetDefaultPresetForWeaponClass(weapon->WeaponClass, &defaultPreset);
		defaultConfig = loadoutInfo->BuildWeaponConfigInfoFromPreset(defaultPreset);
	}

	// get default instance of weapon class
	AFoxWeapon* defaultWeapon = (AFoxWeapon*)UObject::GetDefaultInstanceOf(weapon->WeaponClass);

	// macro which checks if weapon mod is disabled or not supported using mod class and reset it to default if so
#define CHECK_RESET_WPN_MOD(name) \
	if(!defaultWeapon->bSupports##name##Mods && weapon->Weapon##name != nullptr) { \
		Log->warn("reset " #name " for {} weapon in {}: weapon {} does not support" #name "s", wpnTypeStr, playerLoadoutId, weaponName); \
		weapon->Weapon##name = nullptr; \
		loadoutInfo->bNetDirty = true; \
	} else if(defaultWeapon->bSupports##name##Mods && weapon->Weapon##name != nullptr) { \
		bool resetMod = false; \
		std::string resetReason; \
		if(IsItemDisabled(weapon->Weapon##name)) {\
			resetReason = fmt::format("weapon mod {} is disabled for weapon {}", weapon->Weapon##name##->GetName(), weaponName); \
			resetMod = true; \
		} else if(!IsModValidForWeapon(weapon->Weapon##name, defaultWeapon)) { \
			resetReason = fmt::format("weapon {} does not support mod {}", weaponName, weapon->Weapon##name##->GetName()); \
			resetMod = true; \
		} \
		if(resetMod) { \
			Log->warn("reset " #name " for {} weapon in {}: {}", wpnTypeStr, playerLoadoutId, resetReason); \
			weapon->Weapon##name = defaultConfig.Weapon##name; \
			hasChanged = true; \
		} \
	 }

	// macro which checks if weapon mod is disabled or not supported using provider id and reset it to default if so
#define CHECK_RESET_WPN_MOD_ID(name) \
	if(!defaultWeapon->bSupports##name##Mods && weapon->Weapon##name != 255) { \
		Log->warn("reset " #name " for {} weapon in {}: weapon {} does not support" #name "s", wpnTypeStr, playerLoadoutId, weaponName); \
		weapon->Weapon##name = 255; \
		loadoutInfo->bNetDirty = true; \
	} else if(defaultWeapon->bSupports##name##Mods && weapon->Weapon##name != 255) { \
		auto prov = reinterpret_cast<UFoxDataProvider_ModBase*>(uds->##name##ProviderArray[weapon->Weapon##name]); \
		bool resetMod = false; \
		std::string resetReason; \
		if(IsItemDisabled(prov->UnlockID)) { \
			resetReason = fmt::format("weapon mod {} is disabled for weapon {}", prov->FriendlyName, weaponName); \
			resetMod = true; \
		} else if(!IsModValidForWeapon(prov, defaultWeapon)) { \
			resetReason = fmt::format("weapon {} does not support {}", weaponName, prov->FriendlyName); \
			resetMod = true; \
		} \
		if(resetMod) { \
			Log->warn("reset " #name " for {} weapon in {}: {}", wpnTypeStr, playerLoadoutId, resetReason); \
			weapon->Weapon##name = defaultConfig.Weapon##name; \
			hasChanged = true; \
		} \
	}

	// check all weapon mods and reset to default if necessary
	CHECK_RESET_WPN_MOD(Barrel);
	CHECK_RESET_WPN_MOD_ID(Muzzle);
	CHECK_RESET_WPN_MOD(Scope);
	CHECK_RESET_WPN_MOD(Stock);
	CHECK_RESET_WPN_MOD(Grip);
	CHECK_RESET_WPN_MOD_ID(Magazine);
	CHECK_RESET_WPN_MOD(Ammo);

	return hasChanged;
}

bool LoadoutValidator::ValidateLoadoutGear(AFoxPC* pc, FProfileGearInfo& gear)
{
	Log->debug("validating loadout gear");
	static auto uds = UObject::GetInstanceOf<UFoxDataStore_Unlockables>();
	
	// @todo move this to initialization (uds->(Gear)ProviderArray is empty at startup, have to figure out when its provisioned)
	static bool gotDefaultArmorIds = false;
	if (!gotDefaultArmorIds) {
		DefaultArmorIds[EArmorItem::EAIT_UpperBody] = GetSuitableGearFromProviderArray(uds->UpperBodyProviderArray);
		DefaultArmorIds[EArmorItem::EAIT_LowerBody] = GetSuitableGearFromProviderArray(uds->LowerBodyProviderArray);
		DefaultArmorIds[EArmorItem::EAIT_Helmet] = GetSuitableGearFromProviderArray(uds->HelmetProviderArray);
	}

	bool hasChanged = false;

	// simple macro that reset gear items to 255 (no item) if disabled
#define CHECK_RESET_GEAR(name, type) \
	if(gear.##name##ID != 255) { \
		auto name##Provider = (UFoxDataProvider_GearInfo*)uds->##type##ProviderArray[gear.##name##ID]; \
		if(IsItemDisabled(##name##Provider->UnlockID)) { \
			Log->warn("reset gear " #name ": {} is disabled", ##name##Provider->FriendlyName); \
			gear.##name##ID = 255; \
			hasChanged = true; \
		} \
	}
	
	// check all gear items and reset if necessary
	CHECK_RESET_GEAR(UpperBody, UpperBody);
	CHECK_RESET_GEAR(LowerBody, LowerBody);
	CHECK_RESET_GEAR(Helmet, Helmet);
	CHECK_RESET_GEAR(Gear_R1, Attachment);
	CHECK_RESET_GEAR(Gear_R2, Attachment);
	CHECK_RESET_GEAR(Gear_L1, Attachment);
	CHECK_RESET_GEAR(Gear_L2, Attachment);
	CHECK_RESET_GEAR(Tactical, Tactical);

	// set armor gear to default if unset
	if (gear.UpperBodyID == 255) {
		gear.UpperBodyID = DefaultArmorIds[EArmorItem::EAIT_UpperBody];
		hasChanged = true;
	}
	if (gear.LowerBodyID == 255) {
		gear.LowerBodyID = DefaultArmorIds[EArmorItem::EAIT_LowerBody];
		hasChanged = true;
	}
	if (gear.HelmetID == 255) {
		gear.HelmetID = DefaultArmorIds[EArmorItem::EAIT_Helmet];
		hasChanged = true;
	}

	// calculate available gear slots
	int availableGearSlots = 0;
	availableGearSlots += ((UFoxDataProvider_Gear_UpperBody*)uds->UpperBodyProviderArray[gear.UpperBodyID])->PawnMods.GearSlots;
	availableGearSlots += ((UFoxDataProvider_Gear_LowerBody*)uds->LowerBodyProviderArray[gear.LowerBodyID])->PawnMods.GearSlots;
	Log->debug("available gear slots: {}", availableGearSlots);

	// reset unavailable gear slots to 255 (no item)
	for (int currGearIndex = EGearType::GearType_MAX - 1; currGearIndex >= availableGearSlots; --currGearIndex) {
		switch (currGearIndex) {
		case EGearType::GearType_R1:
			gear.Gear_R1ID = 255;
			Log->warn("remove gear R1");
			hasChanged = true;
			break;
		case EGearType::GearType_L1:
			gear.Gear_L1ID = 255;
			Log->warn("remove gear L1");
			hasChanged = true;
			break;
		case EGearType::GearType_R2:
			gear.Gear_R2ID = 255;
			Log->warn("remove gear R2");
			hasChanged = true;
			break;
		case EGearType::GearType_L2:
			gear.Gear_L2ID = 255;
			Log->warn("remove gear L2");
			hasChanged = true;
			break;
		}
	}

	return hasChanged;
}

bool LoadoutValidator::IsModValidForWeapon(UClass* modCls, AFoxWeapon* weapon)
{
	auto modUidIt = ItemClassUIDMap.find(modCls);
	if (modUidIt == ItemClassUIDMap.end()) {
		Log->warn("weapon mod class {} is not mapped", modCls->GetName());
		return false;
	}

	return IsModValidForWeapon(modUidIt->second, weapon);
}

bool LoadoutValidator::IsModValidForWeapon(int UID, AFoxWeapon* weapon)
{
	auto modValidInfoIt = ModValidationInfo.find(UID);
	if (modValidInfoIt == ModValidationInfo.end()) {
		Log->warn("weapon mod {} is not mapped", UID);
		return false;
	}

	int weaponUID = weapon->UnlockID;

	return std::find(modValidInfoIt->second.begin(), modValidInfoIt->second.end(), weaponUID) 
		!= modValidInfoIt->second.end();
}

bool LoadoutValidator::IsModValidForWeapon(UFoxDataProvider_ModBase* provider, AFoxWeapon* weapon)
{
	return IsModValidForWeapon(provider->UnlockID, weapon);
}

bool LoadoutValidator::IsItemDisabled(UClass* itemClass)
{
	auto itemUidIt = ItemClassUIDMap.find(itemClass);
	if (itemUidIt == ItemClassUIDMap.end()) {
		Log->warn("item class {} is not mapped", itemClass->GetName());
		return false;
	}

	return IsItemDisabled(itemUidIt->second);
}

bool LoadoutValidator::IsItemDisabled(int uid)
{
	return std::find(validationRules.DisabledItems.begin(), validationRules.DisabledItems.end(), uid) 
		!= validationRules.DisabledItems.end();
}

bool LoadoutValidator::IsModDisabledForWeapon(int modUid, AFoxWeapon* weapon)
{
	auto disabledModMapIt = validationRules.DisabledWeaponMods.find(weapon->UnlockID);

	if (disabledModMapIt == validationRules.DisabledWeaponMods.end())
		return false;

	return std::find(disabledModMapIt->second.begin(), disabledModMapIt->second.end(), modUid) 
		!= disabledModMapIt->second.end();
}

bool LoadoutValidator::IsModDisabledForWeapon(UClass* modCls, AFoxWeapon* weapon)
{
	auto modClsIt = ItemClassUIDMap.find(modCls);

	if (modClsIt == ItemClassUIDMap.end()) {
		Log->warn("mod class {} is not mapped", modCls->GetName());
		return true;
	}

	return IsModDisabledForWeapon(modClsIt->second, weapon);
}

FFoxWeaponPresetInfo LoadoutValidator::GetSuitableWeaponPreset(EWeaponType type)
{
	static auto uds = UObject::GetInstanceOf<UFoxUnlockInfo>(true);
	for (auto& defaultPresetInfo : uds->DefaultWeaponPresets) {
		auto weapon = WeaponNameMap[defaultPresetInfo.Wpn.ToChar()];
		if (weapon->WeaponType == type && !IsItemDisabled(weapon->UnlockID))
			return defaultPresetInfo;
	}

	return uds->DefaultWeaponPresets[0];
}

int LoadoutValidator::GetSuitableGearFromProviderArray(TArray<UUIResourceDataProvider*> providers)
{
	for (int i = 0; i < providers.Count; i++) {
		auto prov = reinterpret_cast<UFoxDataProvider_GearInfo*>(providers[i]);
		if (!IsItemDisabled(prov->UnlockID))
			return i;
	}

	return 255;
}